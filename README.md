## Käytin node versiota 16 ja pakettimanagerina oli Yarn. Pitäisi toimia kaikilla node versioilla >= 16.0.0

Kaikissa kansioissa on nvm käytössä, eli sillä saa käyttöön oikean node-version. Jos jossain vaiheessa herjaa node versiosta, niin:
- aja komentorivillä ```nvm use 16.0.0```, tai mikä versionumero nodesta nyt onkaan asennettu koneelle (vaatii, että nvm on asennettu)
- Jos herjaa, ettei oikea node-versiota ole asennettu, niin asenna noden versio 16 komennolla ```nvm install 16.0.0```.
- Vaihtoehtoisesti voi asentaa manuaalisesti jonkin node 16 version

## Käyttöliittymän käynnistys:

Avaa komentorivi-ikkuna projektihakemistoon ja aja seuraavat komennot:
- ```cd client```
- ```yarn install```
- ```yarn start```

Käynnistyy porttiin 4001, se pitää olla vapaana.

Jos herjaa portista, niin korjaus tähän:
- Muuta ```.env``` -tiedostosta porttinumeroa johonkin vapaaseen porttiin
- Käynnistä käyttöliittymä uudestaan

#### Käyttöliittymätestit
Aja komentorivillä
- ```yarn test```

-> Testaa, että komponentit renderöityy sekä ui:ssa käytetyt apufunktiot

## Palvelimen käynnistys:

Avaa uusi komentorivi-ikkuna projektihakemistoon ja aja seuraavat komennot
- ```cd server```
- ```yarn install```
- ```yarn start```

Serveri on käynnistynyt oikein, kun komentoriville tulostuu:
- Connected to MongoDB
- server started at http://localhost:4002


Käynnistyy porttiin 4002, se pitää olla vapaana.

Jos herjaa portista, niin korjaus tähän:
- Muuta ```server/src/server.ts``` -> const PORT = xxxx
- Muuta ```client/src/api/config.ts``` -> ```export const COFFEE_API_ADDRESS = 'http://localhost:xxxx'```
- Käynnistä serveri sekä käyttöliittymä uudestaan

xxxx kuvaa vapaata porttia, joka halutaan asettaa serverin käyttöön

#### Serverin testit
Aja komentorivillä
- ```yarn test```

-> Testaa uuden kahvin luonnin ja kahvien hakemisen. Käyttää erillistä testi-tietokantaa, joka tyhjennetään testien ajon jälkeen.
-> Käyttää oletuksena porttia 4010 (voi vaihtaa ```test/testConfig.ts``` -> ```TEST_PORT = xxxx```)

Testit tulostelee erroreita, vaikka kaikki menee läpi, koska virhetapauksissa tehdään tulostuksia consoleen (Ei elegantein tapa, mutta tässä pienessä projektissa menettelee).
