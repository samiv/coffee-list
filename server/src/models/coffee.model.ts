import mongoose from 'mongoose'

const Schema = mongoose.Schema

const coffeeSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    weightInGram: {
      type: Number,
      required: true
    },
    roastLevel: {
      type: Number,
      required: true
    },
    priceInCents: {
      type: Number,
      required: true
    }
  },
  { timestamps: true }
)

export const Coffee = mongoose.model('Coffee', coffeeSchema)
