import cors from 'cors'
import express from 'express'
import { coffeeRouter } from '../routes/coffeeRouter'

export const createServer = () => {
  const app = express()
  app.use(cors())
  app.use(express.json())
  app.use(coffeeRouter)
  return app
}
