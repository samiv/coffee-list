import mongoose from 'mongoose'

export interface ICoffeeItem {
  _id: mongoose.Schema.Types.ObjectId | null
  name: string
  weightInGram: number
  priceInCents: number
  roastLevel: number
}

export enum ROAST_LEVEL {
  ONE = 1,
  TWO = 2,
  THREE = 3,
  FOUR = 4,
  FIVE = 5
}
