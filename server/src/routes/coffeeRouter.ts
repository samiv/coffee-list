import express from 'express'
import { Coffee } from '../models/coffee.model'

export const coffeeRouter = express.Router()

coffeeRouter.get('/coffee', (_req, res) => {
  Coffee.find()
    .then((coffees) => {
      console.log('returning: ' + JSON.stringify(coffees))
      res.status(200).json(coffees)
    })
    .catch((err) => {
      console.error('Error: ' + err)
      res.status(400).json('Error: ' + err)
    })
})

coffeeRouter.post('/coffee', (req, res) => {
  const name = req.body.name
  const priceInCents = req.body.priceInCents
  const weightInGram = req.body.weightInGram
  const roastLevel = req.body.roastLevel

  if (roastLevel < 1 || roastLevel > 5) {
    res.status(400).json('Roast level out of range. It should be in range 1 - 5')
    console.error('Error: ' + 'Roast level out of range. It should be in range 1 - 5')
    return
  }

  const newCoffee = new Coffee({
    name,
    priceInCents,
    weightInGram,
    roastLevel
  })

  newCoffee
    .save()
    .then(() => {
      console.log('Coffee added!')
      res.status(201).json('Coffee added')
    })
    .catch((err) => {
      console.error('Error: ' + err)
      res.status(400).json('Error: ' + err)
    })
})
