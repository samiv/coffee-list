import mongoose from 'mongoose'

import { createServer } from './utils/serverUtils'

const mongoURL =
  'mongodb+srv://kahvitesti:kahvitellaan@coffeecluster.nj4bs.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'

mongoose
  .connect(mongoURL)
  .then(() => {
    const app = createServer()
    // start the Express server
    app.listen(4002, () => {
      console.log(`server started at http://localhost:4002}`)
    })
    console.log('connected to MongoDB')
  })
  .catch((error) => {
    console.error('error connection to MongoDB:', error.message)
  })

const closeProcess = () => {
  mongoose.connection.close(() => {
    process.exit(0)
  })
}

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', closeProcess).on('SIGTERM', closeProcess)
