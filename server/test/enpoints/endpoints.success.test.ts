import { ICoffeeItem, ROAST_LEVEL } from '../../src/types/coffeeTypes'
import mongoose from 'mongoose'
import supertest from 'supertest'

import { Coffee } from '../../src/models/coffee.model'
import { createServer } from '../../src/utils/serverUtils'
import { TEST_MONGODB_URL, TEST_PORT } from '../testConfig'

describe('coffee endpoint SUCCESS tests', () => {
  const app = createServer()

  // start the Express server
  const server = app.listen(TEST_PORT, () => {
    console.log(`server started at http://localhost:${TEST_PORT}`)
  })

  // Delete all created data
  beforeAll(async () => {
    await mongoose
      .connect(TEST_MONGODB_URL)
      .then(() => {
        console.log('TEST - connected to MongoDB')
      })
      .catch((error) => {
        console.error('TEST - error connection to MongoDB:', error.message)
      })
  })

  afterAll(async () => {
    // Close express server
    server.close()

    // Closing the DB connection allows Jest to exit successfully.
    await mongoose.disconnect()
  })

  afterEach(async () => {
    // Delete test database data
    await Coffee.deleteMany({})
  })

  const testCoffee1: ICoffeeItem = {
    _id: null,
    name: 'Testinen Kahvi',
    priceInCents: 490,
    weightInGram: 600,
    roastLevel: ROAST_LEVEL.ONE
  }

  const testCoffee2: ICoffeeItem = {
    _id: null,
    name: 'Testinen Kahvi2',
    priceInCents: 1490,
    weightInGram: 1600,
    roastLevel: ROAST_LEVEL.FIVE
  }

  it.each([testCoffee1, testCoffee2])('tests creating new coffee: POST /coffee', async (coffee) => {
    await supertest(app).post('/coffee').send(coffee).expect(201)
  })

  it('tests GET /coffee', async () => {
    await supertest(app).post('/coffee').send(testCoffee1).expect(201)
    await supertest(app).post('/coffee').send(testCoffee2).expect(201)

    await supertest(app)
      .get('/coffee')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(async (response) => {
        expect(Array.isArray(response.body)).toBeTruthy()
        expect(response.body.length).toEqual(2)

        // Check the response data
        expect(response.body[0]._id).toBeTruthy()
        expect(response.body[0].name).toBe(testCoffee1.name)
        expect(response.body[0].priceInCents).toBe(testCoffee1.priceInCents)
        expect(response.body[0].roastLevel).toBe(testCoffee1.roastLevel)

        expect(response.body[1]._id).toBeTruthy()
        expect(response.body[1].name).toBe(testCoffee2.name)
        expect(response.body[1].priceInCents).toBe(testCoffee2.priceInCents)
        expect(response.body[1].roastLevel).toBe(testCoffee2.roastLevel)
      })
  })
})
