import { ROAST_LEVEL } from '../../src/types/coffeeTypes'
import supertest from 'supertest'

import { createServer } from '../../src/utils/serverUtils'
import { TEST_PORT } from '../testConfig'

describe('coffee endpoint FAILURE tests', () => {
  const app = createServer()

  // start the Express server
  const server = app.listen(TEST_PORT, () => {
    console.log(`server started at http://localhost:${TEST_PORT}`)
  })

  afterAll(async () => {
    // Close express server
    server.close()
  })

  const invalidObject1 = {
    _id: null,
    priceInCents: 490,
    weightInGram: 600,
    roastLevel: ROAST_LEVEL.ONE
  }

  const invalidObject2 = {
    name: 'Testinen Kahvi2',
    priceInCents: 1490,
    weightInGram: 1600
  }

  const invalidObject3 = {
    _id: null,
    name: 'Testinen Kahvi2',
    priceInCents: 1490,
    weightInGram: 1600,
    roastLevel: 6
  }

  it.each([invalidObject1, invalidObject2, invalidObject3])(
    'tests creating new coffee with invalid object: POST /coffee',
    async (object) => {
      await supertest(app).post('/coffee').send(object).expect(400)
    }
  )
})
