import { Box } from '@mui/material'
import { makeStyles } from '@mui/styles'

import { ICoffeeItem } from '../types/coffeeTypes'
import { getWeightString } from '../util/helpers'

const useStyles = makeStyles({
  coffeeInfoContainer: {
    background:
      'linear-gradient(145deg, rgba(18,11,124,1) 0%, rgba(15,15,162,1) 54%, rgba(20,144,201,1) 92%)',
    color: '#fff',
    width: '350px',
    fontFamily: `'Fira Sans Condensed', sans-serif`,
    fontSize: 'px',
    fontWeight: '600',
    marginBottom: '30px',
    padding: '20px',
    borderRadius: '8px'
  },
  coffeeTitle: {
    fontSize: '30px',
    fontWeight: '600',
    fontFamily: `'Indie Flower', cursive`
  }
})

interface ICoffeeInfoProps {
  coffeeItem: ICoffeeItem
}

const CoffeeInfoBox = (props: ICoffeeInfoProps) => {
  const classes = useStyles()
  return (
    <Box className={classes.coffeeInfoContainer}>
      <Box className={classes.coffeeTitle}>{props.coffeeItem.name}</Box>
      <Box>{`Paino: ${getWeightString(props.coffeeItem.weightInGram)}`}</Box>
      <Box>{`Paahtoaste: ${props.coffeeItem.roastLevel} / 5`}</Box>
      <Box>{`Hinta: ${props.coffeeItem.priceInCents.toFixed(2)} e`}</Box>
    </Box>
  )
}

export default CoffeeInfoBox
