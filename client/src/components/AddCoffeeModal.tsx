import { Box, Button, TextField } from '@mui/material'
import { makeStyles } from '@mui/styles'
import Modal from '@mui/material/Modal'
import Autocomplete from '@mui/material/Autocomplete'
import CircularProgress from '@mui/material/CircularProgress'

import { ROAST_LEVELS } from '../types/coffeeTypes'
import { useState } from 'react'
import { useCreateCoffee } from '../api/coffeeApi'

const useStyles = makeStyles({
  modalContainer: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '600px',
    backgroundColor: '#fff',
    border: '1px solid #202020',
    padding: '30px 0',
    borderRadius: '4px'
  },
  modalContent: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  modalTitle: {
    fontSize: '40px',
    fontFamily: `'Indie Flower', cursive`,
    marginBottom: '16px'
  },
  field: {
    marginTop: '24px'
  },
  buttonRow: {
    display: 'flex',
    justifyContent: 'space-around',
    width: '220px'
  },
  error: {
    marginTop: '16px',
    height: '20px',
    color: '#d44987'
  },
  loadingIndicator: {
    height: '50px'
  }
})

interface IAddCoffeeModalProps {
  isOpen: boolean
  setIsOpen: (open: boolean) => void
}

export const MODAL_TITLE_TEXT = 'Lisää uusi lempparikahvi'

const AddCoffeeModal = (props: IAddCoffeeModalProps) => {
  const classes = useStyles()

  const [name, setName] = useState<string>()
  const [weightInGram, setWeightInGram] = useState<number>()
  const [priceInCents, setPriceInCents] = useState<number>()
  const [roastLevel, setRoastLevel] = useState<number | null>()

  const clearFields = () => {
    setName(undefined)
    setWeightInGram(undefined)
    setPriceInCents(undefined)
    setRoastLevel(undefined)
  }

  const [error, setError] = useState<string>('')

  const handleClose = () => {
    props.setIsOpen(false)
    clearFields()
  }

  const { mutate: createCoffee, isLoading } = useCreateCoffee(handleClose)

  return (
    <Modal
      open={props.isOpen}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box className={classes.modalContainer}>
        <Box className={classes.modalContent}>
          <Box className={classes.modalTitle}>{MODAL_TITLE_TEXT}</Box>
          <Box className={classes.field}>
            <TextField
              id="name"
              label="Nimi"
              variant="outlined"
              value={name}
              onChange={(e) => {
                setName(e.target.value)
                setError('')
              }}
              focused
            />
          </Box>
          <Box className={classes.field}>
            <TextField
              id="weight"
              label="Paino grammoina"
              variant="outlined"
              type="number"
              value={weightInGram}
              onChange={(e) => {
                const value = Number(e.target.value)
                setWeightInGram(value === 0 ? undefined : value)
                setError('')
              }}
              focused
            />
          </Box>
          <Box className={classes.field}>
            <TextField
              id="price"
              label="Hinta Euroissa"
              variant="outlined"
              type="number"
              focused
              value={priceInCents}
              onChange={(e) => {
                const value = Number(e.target.value)
                setPriceInCents(value === 0 ? undefined : value)
                setError('')
              }}
            />
          </Box>
          <Box className={classes.field}>
            <Autocomplete
              id="roastLevel"
              sx={{ width: 200 }}
              options={ROAST_LEVELS}
              value={roastLevel}
              onChange={(_event, newValue) => {
                setRoastLevel(newValue)
                setError('')
              }}
              renderInput={(params) => <TextField {...params} label="Paahtoaste (1-5)" focused />}
            />
          </Box>
          <Box className={classes.error}>{error}</Box>

          <Box className={classes.loadingIndicator}>{isLoading && <CircularProgress />}</Box>

          <Box className={classes.buttonRow}>
            <Button onClick={handleClose} variant="contained" color="secondary">
              Peruuta
            </Button>
            <Button
              onClick={() => {
                if (!name || name.length === 0 || name.length > 30) {
                  setError('Nimi ei voi olla tyhjä, eikä yli 30 merkkiä')
                } else if (!weightInGram || weightInGram <= 0) {
                  setError('Painon pitää olla suurempi kuin nolla')
                } else if (!priceInCents || priceInCents <= 0) {
                  setError('Hinnan pitää olla suurempi kuin nolla')
                } else if (!roastLevel || roastLevel <= 0 || roastLevel > 5) {
                  setError('Paahtoasteen pitää olla numero väliltä 1-5')
                } else {
                  createCoffee({
                    _id: null,
                    name,
                    weightInGram,
                    priceInCents,
                    roastLevel
                  })
                  clearFields()
                  setError('')
                }
              }}
              variant="contained"
            >
              Lisää
            </Button>
          </Box>
        </Box>
      </Box>
    </Modal>
  )
}

export default AddCoffeeModal
