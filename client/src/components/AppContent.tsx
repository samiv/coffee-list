import { Box } from '@mui/material'
import Button from '@mui/material/Button'
import { makeStyles } from '@mui/styles'
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline'
import CircularProgress from '@mui/material/CircularProgress'
import { useState } from 'react'

import CoffeeInfoBox from './CoffeeInfoBox'
import AddCoffeeModal from './AddCoffeeModal'
import { useGetCoffees } from '../api/coffeeApi'

const useStyles = makeStyles({
  contentContainer: {
    fontFamily: `'Fira Sans Condensed', sans-serif`,
    display: 'flex',
    flexDirection: 'column',
    marginTop: '80px',
    width: '800px'
  },
  addButton: {
    width: '140px',
    alignSelf: 'center'
  },
  coffeesContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: '60px'
  },
  loadingIndicator: {
    marginTop: '100px',
    fontSize: '60px',
    fontFamily: `'Fira Sans Condensed', sans-serif`
  }
})

export const ADD_NEW_BUTTON_TEXT = 'Lisää uusi'
export const LOADING_TEXT = 'Ladataan'

const AppContent = () => {
  const { coffees, isLoading } = useGetCoffees()

  const [addCoffeeModalOpen, setAddCoffeeModalOpen] = useState<boolean>(false)

  const classes = useStyles()

  if (isLoading) {
    return (
      <Box className={classes.loadingIndicator}>
        <Box>{LOADING_TEXT}</Box>
        <CircularProgress />
      </Box>
    )
  }
  return (
    <Box className={classes.contentContainer}>
      <Button
        className={classes.addButton}
        variant="contained"
        color="secondary"
        startIcon={<AddCircleOutlineIcon />}
        onClick={() => setAddCoffeeModalOpen(true)}
      >
        {ADD_NEW_BUTTON_TEXT}
      </Button>

      <Box className={classes.coffeesContainer}>
        {coffees.map((c) => {
          return <CoffeeInfoBox coffeeItem={c} />
        })}
      </Box>

      <AddCoffeeModal isOpen={addCoffeeModalOpen} setIsOpen={setAddCoffeeModalOpen} />
    </Box>
  )
}

export default AppContent
