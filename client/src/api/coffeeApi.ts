import axios from 'axios'
import { useQuery, useMutation, useQueryClient } from 'react-query'
import { ICoffeeItem } from '../types/coffeeTypes'

import { COFFEE_API_ADDRESS } from './config'

const GET_COFFEES_QUERY_KEY = 'getCoffees'

// ------ GET COFFEES ------
export const useGetCoffees = () => {
  const result = useQuery<ICoffeeItem[], Error>(GET_COFFEES_QUERY_KEY, () => getCoffees())

  return {
    ...result,
    coffees: result.data || []
  }
}

const getCoffees = async () => {
  const result = await axios.get<ICoffeeItem[]>(`${COFFEE_API_ADDRESS}/coffee`)
  return result.data
}

// ------ CREATE COFFEE ------

export const useCreateCoffee = (closeModal: () => void) => {
  const queryClient = useQueryClient()

  return useMutation(createCoffee, {
    onSuccess: () => {
      queryClient.invalidateQueries(GET_COFFEES_QUERY_KEY)
      closeModal()
    }
  })
}

const createCoffee = async (coffeeToCreate: ICoffeeItem) => {
  const result = await axios.post(`${COFFEE_API_ADDRESS}/coffee`, coffeeToCreate)
  return result.data
}
