import { Box } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { QueryClient, QueryClientProvider } from 'react-query'

import AppContent from './components/AppContent'

const useStyles = makeStyles({
  title: {
    fontSize: '70px',
    fontFamily: `'Indie Flower', cursive`,
    marginTop: '20px'
  },
  appContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  body: {
    fontFamily: `'Fira Sans Condensed', sans-serif`
  }
})

export const APP_TITLE = 'Samin Lempparikahvit'

const queryClient = new QueryClient()

function App() {
  const classes = useStyles()
  return (
    <QueryClientProvider client={queryClient}>
      <Box className={classes.appContainer}>
        <Box className={classes.title}>{APP_TITLE}</Box>
        <AppContent />
      </Box>
    </QueryClientProvider>
  )
}

export default App
