import mongoose from 'mongoose'

export interface ICoffeeItem {
  _id: mongoose.Schema.Types.ObjectId | null
  name: string
  weightInGram: number
  priceInCents: number
  roastLevel: number
}
export const ROAST_LEVELS = [1, 2, 3, 4, 5]
