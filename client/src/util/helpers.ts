export const getWeightString = (weightInGram: number) =>
  weightInGram < 1000 ? `${weightInGram} g` : `${weightInGram / 1000} kg`
