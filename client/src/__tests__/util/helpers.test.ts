import { getWeightString } from './../../util/helpers'

describe('test getWeightString function', () => {
  const validWeights: any[] = [
    [932, '932 g'],
    [1234, '1.234 kg'],
    [1, '1 g'],
    [123123123, '123123.123 kg'],
    [0, '0 g'],
    [1001, '1.001 kg'],
    [0.01, '0.01 g']
  ]

  it.each(validWeights)('test valid casting', (weight, expected) => {
    expect(getWeightString(weight)).toBe(expected)
  })
})

describe('to be fixed for showing price', () => {
  const prices: any[] = [
    [1123, '1123.00'],
    [0.1, '0.10'],
    [0.0001, '0.00'],
    [1.1, '1.10'],
    [0.8, '0.80'],
    [100000000.0, '100000000.00'],
    [0.009, '0.01'],
    [0.04, '0.04']
  ]

  it.each(prices)('test success casting', (value, expected) => {
    expect(value.toFixed(2)).toBe(expected)
  })
})
