import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'

import CoffeeInfoBox from '../../components/CoffeeInfoBox'
import { ICoffeeItem } from '../../types/coffeeTypes'
import { getWeightString } from '../../util/helpers'

describe('CoffeeInfoBox component', () => {
  const testCoffeeItem: ICoffeeItem = {
    _id: null,
    name: 'testinimi',
    weightInGram: 1234,
    priceInCents: 9999,
    roastLevel: 4
  }
  test('renders CoffeeInfoBox component', () => {
    render(<CoffeeInfoBox coffeeItem={testCoffeeItem} />)
    expect(
      screen.getByText(`Paino: ${getWeightString(testCoffeeItem.weightInGram)}`)
    ).toBeInTheDocument()
    expect(screen.getByText(testCoffeeItem.name)).toBeInTheDocument()
    expect(screen.getByText(`Paahtoaste: ${testCoffeeItem.roastLevel} / 5`)).toBeInTheDocument()
    expect(screen.getByText(`Hinta: ${testCoffeeItem.priceInCents.toFixed(2)} e`)).toBeInTheDocument()
  })
})
