import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'

import AddCoffeeModal, { MODAL_TITLE_TEXT } from '../../components/AddCoffeeModal'
import { QueryClient, QueryClientProvider } from 'react-query'

describe('AddCoffeeModal component', () => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        retry: false
      }
    }
  })
  test('renders AddCoffeeModal component', () => {
    render(
      <QueryClientProvider client={queryClient}>
        <AddCoffeeModal isOpen={true} setIsOpen={() => {}} />
      </QueryClientProvider>
    )
    expect(screen.getByText(MODAL_TITLE_TEXT)).toBeInTheDocument()
  })
})
