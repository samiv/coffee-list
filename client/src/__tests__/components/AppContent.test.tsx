import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import AppContent, { LOADING_TEXT } from '../../components/AppContent'
import { QueryClient, QueryClientProvider } from 'react-query'

describe('AppContent component', () => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        retry: false
      }
    }
  })
  test('renders AppContent component', () => {
    render(
      <QueryClientProvider client={queryClient}>
        <AppContent />
      </QueryClientProvider>
    )
    expect(screen.getByText(LOADING_TEXT)).toBeInTheDocument()
  })
})
