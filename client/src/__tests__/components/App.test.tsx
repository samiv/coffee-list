import { render, screen } from '@testing-library/react'
import App, { APP_TITLE } from '../../App'
import '@testing-library/jest-dom'

describe('App', () => {
  test('renders App component', () => {
    render(<App />)
    expect(screen.getByText(APP_TITLE)).toBeInTheDocument()
  })
})
